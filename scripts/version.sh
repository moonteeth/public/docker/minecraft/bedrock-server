#!/bin/sh -eu
USER_AGENT="Mozilla/5.0 Gecko/20100101 Firefox/81.0"
URL=$(curl -s -A "${USER_AGENT}" https://www.minecraft.net/en-us/download/server/bedrock | grep serverBedrockLinux |  sed -e 's/.*<a href=["'"'"']//i' -e 's/["'"'"'].*$//i')
VERSION=$(basename $(echo ${URL} | rev |  cut -d - -f 1 | rev) .zip)

echo "URL=${URL}"
echo "VERSION=${VERSION}"
