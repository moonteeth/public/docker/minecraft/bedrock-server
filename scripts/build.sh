#!/bin/sh -eu
DOCKER_CMD=docker
${DOCKER_CMD} build --build-arg="VERSION=${VERSION}" --build-arg="DOWNLOAD_URL=${URL}" -t bedrock-server:"${VERSION}" docker
${DOCKER_CMD} tag bedrock-server:"${VERSION}" bedrock-server:latest
