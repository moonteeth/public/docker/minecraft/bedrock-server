#!/bin/sh

apt-get update && apt-get -y dist-upgrade

SERVER_DIR="/bedrock-server"
CONFIG_DIR="/config"

CONFIG_FILES="permissions.json allowlist.json server.properties"

mkdir -p "${CONFIG_DIR}/orig"

for i in ${CONFIG_FILES}; do
  if [ "${i}" = "server.properties" ] || [ ! -f "${CONFIG_DIR}/${i}" ]; then
    cp -p "${SERVER_DIR}/${i}" "${CONFIG_DIR}"
  fi
  if [ -f "${SERVER_DIR}/${i}" ] && [ ! -L "${SERVER_DIR}/${i}" ]; then
    mv -f "${SERVER_DIR}/${i}" "${CONFIG_DIR}/orig"
  fi
  ln -sf "${CONFIG_DIR}/${i}" "${SERVER_DIR}/${i}"
done

/config_gen.sh "${CONFIG_DIR}/server.properties"

chown -R minecraft:minecraft "${CONFIG_DIR}" "${SERVER_DIR}/worlds"

exec su minecraft -c "$@"
